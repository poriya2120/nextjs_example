import Layout from '../components/Layout';
import Price from '../components/Price';
import Fetch from 'isomorphic-unfetch';


const Index = (props) => (
  <Layout>
    <div>
      <h1>Welcome to Nextjs</h1>
      <p>check bitcoin to countries</p>
      <Price bpi={props.bpi}/>
    </div>
  </Layout>
);

Index.getInitialProps = async function() {
  const res = await fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
  const data = await res.json();

  return {
    bpi: data.bpi
  };
}

export default Index;