import Navbar from './Navbar';
import Head from 'next/head';

const Layout = (props) => {
    return (  
        <div>
            <Head>
                <title>Nextjs</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.5.0/cerulean/bootstrap.min.css" integrity="sha512-voXLrIWfe6JS/sUNVABmDsu2iKvQCdv08bUYDX0BW5fvkCxLa89MJDQOwOemsjFKATsVLu8Zy2sbvp0gqsWFxQ==" crossorigin="anonymous" />
            </Head>
        <Navbar/>
        <div class="container">
        {props.children}

        </div>
        </div>

    );
}
 
export default Layout;